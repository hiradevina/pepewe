"""cvprofile URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path, re_path, include
from intro.views import about as aboutme
from intro.views import desc as description
from intro.views import msg as message
from intro.views import homepage as home


urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^about', aboutme, name="about me"),
    re_path(r'^desc', description, name="description"),
    re_path(r'^msg', message, name="send message"),
    re_path(r'^homepage', home, name="homes"),
    
]

