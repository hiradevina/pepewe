from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static

urlpatterns = [
    path('', views.homepage, name="home"),
    path('', views.about, name="about"),
    path('', views.msg, name="contact"),
    path('', views.desc, name="desc"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)